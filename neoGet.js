const { session, driver } = require('./creds')
var fs = require('fs')

session.run(`MATCH p=()-[r:ACTED_IN]->() RETURN p LIMIT 25`).then((result) => {
    b = JSON.stringify(result.records, null, 2);
    p = JSON.parse(b)
    k = [p];
    data = [];
    p.forEach(e => {
        let start = e._fields[0].start.identity.low;
        let end = e._fields[0].end.identity.low;
        let z = { 
            from: start,
            to: end 
        };
        data.push(z);
    });

    console.log(data);
})