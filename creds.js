const neo4j = require('neo4j-driver');
uri = "bolt://localhost:7687";
user = "neo4j";
password = "1234";
const driver = neo4j.driver(uri, neo4j.auth.basic(user, password))
const session = driver.session()

module.exports = { session, driver };